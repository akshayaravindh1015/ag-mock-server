# AgMockServer
### _Mock Backend  &  GUI for your swagger(in json format)._

[](https://drive.google.com/uc?export=view&id=11ISv9G7qkNBqH2L0F5DK_FccdxTNwtnM)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/akshayaravindh1015/ag-mock-server/src/master/)

## Features
- Ability to send pre-configured responses for a specific API.
- An editable light GUI for your json file.
- State consistency - even when page refreshes.
- Ability to UPLOAD & DOWNLOAD json file.
- Configurable accross different environments.
- ✨See the Magic after clicking the menu button ✨
> The main goal of AGMockServer is to ease development effort of a UI Developer :)
## Installation
AgMockServer requires [Node.js](https://nodejs.org/) v10+ to run.
```sh
npm install ag-mock-server
```

#### 2-ways to use it :
---
## 1. For Angular Apps

##### AgMockServer is basically an Inteceptor(Angular) which acts a fake bakend:
![Image - Flow diagram of Ag Mock Interceptor as Fake Bakend.](https://firebasestorage.googleapis.com/v0/b/storage-50ed6.appspot.com/o/AG_Mockserver_for_Angular.jpeg?alt=media&token=9a5d757c-772d-4e54-a401-dcc4e232c00f)
### How to use for Angular:
1. Add the following services & interfaces in the providers section of AppModule. That's all dude.. Enjoyy!!
```typescript
...
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { ByPassControllerService, ByPassInterceptor, FakeBackendService } from 'ag-mock-server';

@NgModule({
  ***
  providers: [
    ...(
      environment.production ? // <--can use any flag other than PROD flag.
        [] : [
          FakeBackendService,
          ByPassControllerService,
          {
            provide: HTTP_INTERCEPTORS,
            useClass: ByPassInterceptor,
            multi: true,
          },
          {
            provide: APP_INITIALIZER,
            useFactory: (service: FakeBackendService) => () => { return service.startMockServer(); },
            deps: [FakeBackendService],
            multi: true,
          }
        ]
    ),
  ],
})
export class AppModule { }

```
---
## 2. For All Other Web Apps.
##### AgMockServer is basically mockserver running on port xxxx with configrable apis from json:
![Image - Flow diagram of Ag Mock Interceptor as Fake Bakend.](https://firebasestorage.googleapis.com/v0/b/storage-50ed6.appspot.com/o/AG_Mockserver_for_all_other_web_apps.jpeg?alt=media&token=5d567bfc-0682-4ed5-bbf7-2951d0073d93)
### How to use for all other web pages:
- Developement still in progress - expected release in version 3.0.
***


## Development
Have suggestions or found bugs in the app? Great. Please share your feedback in the below articles:
[Medium](https://medium.com/) || [Hashnode](https://puppysteps.hashnode.dev/) || Connect on [linkedin](https://www.linkedin.com/in/akshay-kumar-pasunuri-91043216a/);

## License
MIT

**Free Software, Hell Yeah!**

