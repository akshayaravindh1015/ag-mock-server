import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
import { ValueComponent } from './value/value.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToggleComponent } from './toggle/toggle.component';
import { HeaderComponent } from './header/header.component';
import { TypeComponent } from './type/type.component';
import { DirectivesModule } from '../common/directives/directives.module';
import { StatusComponent } from './status/status.component';
import { CyberButtonComponent } from './cyber-button/cyber-button.component';
import { ActionButtonComponent } from './action-button/action-button.component';
import { MenuComponent } from './menu/menu.component';
import { OptionsContainerComponent } from './options-container/options-container.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { CheckboxComponent } from './checkbox/checkbox.component';

@NgModule({
  declarations: [
    CardComponent,
    ValueComponent,
    ToggleComponent,
    HeaderComponent,
    TypeComponent,
    StatusComponent,
    CyberButtonComponent,
    ActionButtonComponent,
    MenuComponent,
    OptionsContainerComponent,
    SnackBarComponent,
    CheckboxComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
  ],
  exports: [
    CardComponent,
    ValueComponent,
    ToggleComponent,
    HeaderComponent,
    TypeComponent,
    StatusComponent,
    CyberButtonComponent,
    ActionButtonComponent,
    MenuComponent,
    OptionsContainerComponent,
    SnackBarComponent,
    CheckboxComponent
  ]
})
export class ComponentsModule { }
