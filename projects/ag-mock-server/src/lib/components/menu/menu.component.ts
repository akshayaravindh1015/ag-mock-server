import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit {

  @Output() changed: EventEmitter<boolean> = new EventEmitter(false);
  controller: FormControl = new FormControl(false);
  constructor() { }

  ngOnInit(): void {
  }
  valueCahnged() {
    this.changed.emit(!this.controller.value);
  }

  @HostListener('document:keydown.escape', ['$event'])
  onEsc(event: KeyboardEvent) {
    this.controller.patchValue(false);
    this.changed.emit(false);
  }

}
