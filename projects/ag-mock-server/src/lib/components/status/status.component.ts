import { HttpStatusCode } from '../../common/models';
// import * as https from "@angular/common/http/http.d"
import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, HostListener, ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusComponent implements OnInit {

  @Input() status!: HttpStatusCode;
  @Output() statusChanged: EventEmitter<HttpStatusCode> = new EventEmitter();
  // public statuses = (https as any).HttpStatusCode;
  @Input() statuses = Object.values(HttpStatusCode).filter(val => typeof val == 'number') as Array<number>;
  public isInEditState: boolean = false;

  constructor(
    private _dataServc: DataService,
    // private ref: ChangeDetectorRef,,
  ) { }

  ngOnInit(): void {
    // let values: string[] = Object.keys(HttpStatusCode).map(key => HttpStatusCode[key]).filter(x => !(parseInt(x) >= 0))
  }

  showEditStatusContainer(event: MouseEvent) {
    event.stopImmediatePropagation();
    if (this._dataServc.canEdit) {
      this.isInEditState = true;
    }
    // this.ref.detectChanges();
  }

  selectStatus(event: MouseEvent, statusCode: HttpStatusCode) {
    event.stopPropagation();
    this.isInEditState = false;
    this.statusChanged.emit(statusCode);
    // this.ref.detectChanges();
  }

  @HostListener('window:click')
  onClick() {
    this.isInEditState = false;
  }

}
