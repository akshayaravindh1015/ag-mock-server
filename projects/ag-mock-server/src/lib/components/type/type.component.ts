import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, HostListener, ChangeDetectorRef } from '@angular/core';
import { METHOD } from '../../common/models';
import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypeComponent implements OnInit {

  @Input() type!: METHOD;
  @Output() methodChanged: EventEmitter<METHOD> = new EventEmitter();

  public isInEditState: boolean = false;
  public types = Object.values(METHOD).slice(0, Object.values(METHOD).length - 1);

  constructor(
    private _dataServc: DataService,
    // private ref: ChangeDetectorRef,,
  ) { }

  ngOnInit(): void {
  }

  showEditApiContainer(event: MouseEvent) {
    event.stopImmediatePropagation();
    if (this._dataServc.canEdit) {
      this.isInEditState = true;
    }
    // this.ref.detectChanges();
  }

  selectType(event: MouseEvent, method: METHOD) {
    event.stopPropagation();
    this.isInEditState = false;
    this.methodChanged.emit(method);
    // this.ref.detectChanges();
  }

  @HostListener('window:click')
  onClick() {
    this.isInEditState = false;
  }

}
