import { Component, OnInit, Output, EventEmitter, Input, AfterViewInit } from '@angular/core';

@Component({
  selector: 'checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent implements OnInit, AfterViewInit {

  @Input() initialValue: boolean = false;
  @Input() id!: string;
  value: boolean = false;
  @Output() valuechanged: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    this.value = this.initialValue;
    console.log(this.value);
  }

  checkClicked() {
    this.valuechanged.emit(!this.value);
  }

}
