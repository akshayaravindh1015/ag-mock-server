import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'cyber-button',
  templateUrl: './cyber-button.component.html',
  styleUrls: ['./cyber-button.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class CyberButtonComponent implements OnInit {

  @Input() label!: string;
  constructor() { }

  ngOnInit(): void {
  }

}
