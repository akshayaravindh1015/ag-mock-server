import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  // // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToggleComponent implements OnInit {

  @Input() label!: string;
  @Input() formControl!: FormControl;

  initialVale: boolean = false;
  constructor() { }

  ngOnInit(): void {
    this.initialVale = !!this.formControl.value;
  }

  valueChanged() {
    this.initialVale = !this.initialVale;
    this.formControl.patchValue(this.initialVale);
  }

}
