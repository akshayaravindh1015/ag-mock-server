import { Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Parameter, ValueType, ValueModel } from '../../common/models';
import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'value',
  templateUrl: './value.component.html',
  styleUrls: ['./value.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValueComponent implements OnInit {

  @Input() value!: string;
  prevValue = `${this.value}`;
  @Input() font?: string = '';
  // @Input() resetFormSubject: Observable<boolean> = of(false);

  // subscription: Subscription = this.resetFormSubject
  //   .pipe(
  //     tap((reset) => {
  //       if (reset) {
  //         this.value = this.prevValue;
  //       }
  //     })
  //   ).subscribe();

  // @Input() key!: string;
  @ViewChild("input") inputEl!: ElementRef;
  @ViewChild("textarea") textAreaEl!: ElementRef;
  @Output() valueUpdated: EventEmitter<ValueModel> = new EventEmitter<ValueModel>()

  isInEditState: boolean = false;
  valueType: ValueType = ValueType.STRING;

  constructor(
    private _dataServc: DataService,
    // private ref: ChangeDetectorRef,,
  ) { }

  ngOnInit(): void {
    this.formatData();
  }

  // ngDoCheck() {
  //   // this.value = this.prevValue;
  // }

  formatData() {
    if (this.value) {
      this.valueType = this._dataServc.getValueType(this.value);
      this.isInEditState = false;
      // this.ref.detectChanges();
      this.valueUpdated.emit({ textInside: this.value, valueType: this.valueType });
    }
  }

  startEditing(event: MouseEvent) {
    event.stopPropagation();
    if (this._dataServc.canEdit) {
      this.isInEditState = true;
      // this.ref.detectChanges();
      setTimeout(
        () => {
          if (this.valueType == ValueType.JSON) {
            this.textAreaEl.nativeElement.focus()
          } else {
            this.inputEl.nativeElement.focus();
          }
        },
        200,
      )
    }
  }

  /** Getters and Setters */
  // get dateInside() {
  //   return `${this.value}`;
  // }
  // set dateInside(val: string) {
  //   this.value = val;
  // }
  get isDataJSON(): boolean {
    return this.valueType == ValueType.JSON;
  }
  get dataAsJson() {
    return JSON.parse(this.value);
  }
  get isDataBoolean(): boolean {
    return this.valueType == ValueType.BOOLEAN;
  }
  get isDataNumber(): boolean {
    return this.valueType == ValueType.NUMBER;
  }
  get isDataString(): boolean {
    return this.valueType == ValueType.STRING;
  }

  @HostListener('window:click')
  onClick() {
    this.isInEditState = false;
  }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }

}
