export type SnackBarType = 'error' | 'message' | 'success';

export class SnackContent {
    message: string;
    type: SnackBarType;
    constructor(
        message: string,
        type: SnackBarType = 'message',
    ) {
        this.message = message;
        this.type = type;
    }
}