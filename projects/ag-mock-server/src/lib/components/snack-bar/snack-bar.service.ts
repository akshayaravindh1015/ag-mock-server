import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { SnackBarType, SnackContent } from './snack-bar.model';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  private message: Subject<{ message: string, type: SnackBarType }> = new Subject();
  public message$ = this.message.asObservable();
  constructor() { }

  showMessage(message: string) {
    this.message.next(new SnackContent(message));
  }
  showError(message: string) {
    this.message.next(new SnackContent(message, 'error'));
  }
  showSuccess(message: string) {
    this.message.next(new SnackContent(message, 'success'));
  }
}
