import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { SnackBarService } from './snack-bar.service';

@Component({
  selector: 'snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SnackBarComponent implements OnDestroy {

  public subscription: Subscription;
  show: boolean = false;
  message: string = '';
  extraClass: string = '';

  constructor(private _snackBarServc: SnackBarService) {
    this.subscription = this._snackBarServc.message$
      .pipe(
        tap((snack) => {
          this.message = snack.message;
          this.show = true;
          this.extraClass = snack.type;
        }),
        delay(2500),
        tap(() => this.show = false),
      ).subscribe();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
