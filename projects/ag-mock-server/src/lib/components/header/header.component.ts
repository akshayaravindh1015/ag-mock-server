import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { delay, tap } from 'rxjs/operators';
import { ByPassControllerService } from '../../common/services/by-pass-controller.service';
import { DataService } from '../../common/services/data.service';
import { JsonDownloadService } from '../../common/services/json-download.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  constructor(
    public _dataServc: DataService,
    private _controllerServc: ByPassControllerService,
    private _jsonDownLoad: JsonDownloadService,
    // private ref: ChangeDetectorRef,,
  ) { }

  ngOnInit(): void {
  }

  downloadJSON() {
    this._jsonDownLoad.dynamicDownloadJson();
    // this.ref.detectChanges();
  }

  uploadSwagger() {
  }

  onFileChanged(event: any) {
    this._controllerServc.onFileChanged(event)
      .pipe(
        delay(1000),
        tap(success => {
          if (success) {
            // this.ref.detectChanges();
          }
        })
      ).subscribe().unsubscribe();
  }

  getCopyText() {
    return JSON.stringify(this._dataServc.apisAr);
  }
}
