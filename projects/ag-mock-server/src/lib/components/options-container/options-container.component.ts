import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'options-container',
  templateUrl: './options-container.component.html',
  styleUrls: ['./options-container.component.scss'],
})
export class OptionsContainerComponent implements OnInit {

  @Output() delete: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

  showOptions: boolean = false;

  ngOnInit(): void { }

  timer: any;

  initiateTimer() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(
      () => {
        this.showOptions = true;
      }, 500);
  }

  stopTimer() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.showOptions = false;
  }

  deleteClicked() {
    this.delete.emit(true);
  }

}
