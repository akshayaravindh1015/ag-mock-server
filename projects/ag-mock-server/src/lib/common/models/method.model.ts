export enum METHOD {
    POST = 'POST',
    PUT = 'PUT',
    GET = 'GET',
    DELETE = 'DELETE',
    ADD = 'ADD + ',
}