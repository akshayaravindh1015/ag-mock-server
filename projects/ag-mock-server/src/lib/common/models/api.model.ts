import { Parameter } from "./parameter.model";
import { Response, ResponseHeader } from "./response.model";
import { METHOD } from "./method.model";
import { HttpStatusCode } from "./http-status.model";
import { HttpHeaders } from "@angular/common/http";

export class API {
    endPoint!: string;
    endPointRegEx!: RegExp;
    type!: METHOD;
    body!: string;
    currentResponseCode!: HttpStatusCode;
    parameters!: Parameter[];
    responseHeaders!: ResponseHeader[];
    responses!: Response[];
    // request!: HttpRequest<T>;
    // response!: HttpResponse<T>;

    constructor(
        endPoint: string = '/endpoint/{params_here}',
        type: METHOD = METHOD.ADD,
        currentResponseCode: HttpStatusCode = 200,
        parameters: Parameter[] = [],
        responseHeaders: ResponseHeader[] = [],
        responses: Response[] = [],
        body: string = '{ "payload" : [ {"data" : true }] }',
    ) {
        this.endPoint = endPoint;
        this.endPointRegEx = new RegExp(this.endPoint);
        this.type = type;
        this.currentResponseCode = currentResponseCode;
        this.parameters = parameters;
        this.responses = responses;
        this.responseHeaders = responseHeaders;
        this.body = body;
        if (parameters?.length == 0) {
            this.parameters.push(new Parameter('key', 'value'));
        }
        if (responses?.length == 0) {
            this.responses.push(new Response(200, 'success'));
        }
    }
}