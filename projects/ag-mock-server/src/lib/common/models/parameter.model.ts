import { Value, ValueType } from "./value-type.model";

export class Parameter {
    name!: string;
    // description: string;
    value!: string; //Value
    valueType: ValueType = ValueType.STRING;

    constructor(
        name: string,
        // description:,
        value: string,//Value
        valueType: ValueType = ValueType.DUMMY,
    ) {
        this.name = name;
        this.value = value;
        this.valueType = valueType;
    }
}