export type Value = string | boolean | number;
export enum ValueType {
    STRING = 'STRING',
    BOOLEAN = 'BOOLEAN',
    NUMBER = 'NUMBER',
    JSON = 'JSON',
    DUMMY = 'DUMMY',
}
export interface ValueModel {
    valueType: ValueType;
    textInside: string;
    // [s: string]: string;
}