import { HttpHeaders } from "@angular/common/http";
import { HttpStatusCode } from "../models";

import { ValueType } from "./value-type.model";

export class Response {
    status!: HttpStatusCode;
    body!: string;//Value;
    bodyType: ValueType = ValueType.STRING;
    constructor(
        status: HttpStatusCode,
        body: string,//Value,
        bodyType: ValueType = ValueType.DUMMY,
    ) {
        this.status = status;
        this.body = body;
        this.bodyType = bodyType;
    }

    // public addHeader(key: string, value: string) {
    //     this.headers.append(key, value);
    // }
}
export class ResponseHeader {
    key!: string;
    value!: string;
    isInUse!: boolean;
    constructor(
        key: string,
        value: string,
        isInUse: boolean = false,
    ) {
        this.key = key;
        this.value = value;
        this.isInUse = isInUse;
    }
}