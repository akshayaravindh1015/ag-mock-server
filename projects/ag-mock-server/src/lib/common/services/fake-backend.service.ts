import { ApplicationRef, ComponentFactoryResolver, Injectable, Injector } from '@angular/core';

import { MainComponent } from '../../swagger-ui/main/main.component';
import { ByPassControllerService } from './by-pass-controller.service';
import { AgMockServerComponent } from '../../ag-mock-server.component';
import { LoadMainService } from './load-main.service';

@Injectable({
  providedIn: 'root'
})
export class FakeBackendService {

  constructor(
    private injector: Injector,
    private applicationRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private _byPassControllerServc: ByPassControllerService,
    private _loadMainServc: LoadMainService,
  ) { }

  private btnElement = document.createElement('ag-mock-server-lib-btn');
  private mainElement = document.createElement('ag-mock-server');

  startMockServer() {
    this._byPassControllerServc.getPrevDataIfAny();

    this._loadMainServc.openMockServer$.subscribe(
      (open) => {
        if (open) {
          this.openAPIConfigScreen();
        } else {
          this.closeAPIConfigScreen();
        }
      });

    const btnFactory = this.componentFactoryResolver.resolveComponentFactory(AgMockServerComponent);
    const btnElementComponentRef = btnFactory.create(this.injector, [], this.btnElement);
    const mainFactory = this.componentFactoryResolver.resolveComponentFactory(MainComponent);
    const mainElementComponentRef = mainFactory.create(this.injector, [], this.mainElement);


    this.applicationRef.attachView(btnElementComponentRef.hostView);
    this.applicationRef.attachView(mainElementComponentRef.hostView);

    btnElementComponentRef.instance.closed.subscribe(() => {
      document.body.removeChild(this.btnElement);
      this.applicationRef.detachView(btnElementComponentRef.hostView);
    });
    mainElementComponentRef.instance.closed.subscribe(() => {
      document.body.removeChild(this.btnElement);
      this.applicationRef.detachView(mainElementComponentRef.hostView);
    });

    this.openBtnElement();
  }

  /** Btn element functions */
  openBtnElement() {
    document.body.appendChild(this.btnElement);
  }
  closeBtnElement() {
    document.body.removeChild(this.btnElement);
  }

  /** Main Screen functions */
  openAPIConfigScreen() {
    document.body.appendChild(this.mainElement);
  }
  closeAPIConfigScreen() {
    document.body.removeChild(this.mainElement);
  }

}



/** Prev Using angular/elements
  startMockServer() {

    this._byPassControllerServc.getPrevDataIfAny();

    let injector = this.injector;
    const BtnElement = createCustomElement(AgMockServerComponent, { injector });
    // Register the custom element with the browser.
    customElements.define(this._loadMainServc.btnElTag, BtnElement);

    const MainElement = createCustomElement(MainComponent, { injector });
    customElements.define(this._loadMainServc.mainElTag, MainElement);

    this._loadMainServc.openBtnElement();
  }
*/

