import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadMainService {

  private openMockServerUI_Subj: Subject<boolean> = new Subject();

  constructor() { }

  get openMockServer$(): Observable<boolean> {
    return this.openMockServerUI_Subj.asObservable();
  }

  /** Main Screen functions */
  openAPIConfigScreen() {
    this.openMockServerUI_Subj.next(true);
  }
  closeAPIConfigScreen() {
    this.openMockServerUI_Subj.next(false);
  }

}