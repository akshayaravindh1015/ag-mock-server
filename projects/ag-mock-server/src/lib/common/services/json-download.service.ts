import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class JsonDownloadService {

  constructor(private _dataServc: DataService) { }

  private setting = {
    element: {
      dynamicDownload: null as unknown as HTMLElement,
      // dynamicDownload: HTMLAnchorElement,
    }
  }

  public dynamicDownloadTxt() {
    this.dyanmicDownloadByHtmlTag({
      fileName: 'swagger',
      text: JSON.stringify(this._dataServc.apisAr)
    });
  }

  public dynamicDownloadJson() {
    this.dyanmicDownloadByHtmlTag({
      fileName: 'swagger.json',
      text: JSON.stringify(this._dataServc.apisAr)
    });
  }

  private dyanmicDownloadByHtmlTag(arg: {
    fileName: string,
    text: string
  }) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType = arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/plain';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
    element.setAttribute('download', arg.fileName);

    var event = new MouseEvent("click");
    element.dispatchEvent(event);
  }

}
