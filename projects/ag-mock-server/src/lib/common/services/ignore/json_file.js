const fs = require('fs');
// import fs from 'fs';

function myFun() {
    // window.alert("It is working");
    const jsonfile = require('jsonfile');
    const file = '/tmp/data.json';
    fs.readFile('./customer.json', 'utf8', (err, jsonString) => {
        if (err) {
            console.log("File read failed:", err);
            return;
        }
        console.log('File data:', jsonString);
    });
    jsonfile.readFile(file)
        .then(obj => console.dir(obj))
        .catch(error => console.error(error));
}

// myFun();