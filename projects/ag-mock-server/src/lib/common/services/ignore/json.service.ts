import { Injectable } from '@angular/core';
// const fs = require('fs');
// import * as fs from 'fs';
// import * as jsonFILE from 'jsonfile';
// @ts-ignore
// import fs = require('fs');
// declare const myFun: any;

import * as fs from "fs-extra";

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor() {

    // setTimeout(() => {
    //   myFun();
    // }, 1000);
    try {
      fs.readJsonSync('src/assets/mockContent/apis.json')
    } catch (error) {
      console.log(error);
    }
  }

  /*
  
    constructor() {
      console.log(fs);
      this.readFileTest();
  
      this.jsonReader('./customer.json', (err: Error, customer: any) => {
        if (err) {
          console.log(err)
          return
        }
        console.log(customer.address) => "Infinity Loop Drive"
      })
  
      this.finalTest();
    }
  
    finalTest() {
      try {
        console.log("camehere")
        console.log(JSON.parse(fs.readFileSync('file.json', 'utf-8')))
      } catch (error) {
        console.error(error)
      }
    }
  
    readFileTest() {
    fs.readFile('./customer.json', 'utf8', (err, jsonString) => {
      if (err) {
        console.log("File read failed:", err);
        return;
      }
      console.log('File data:', jsonString);
    });
  
  
    try {
      const jsonString = fs.readFileSync('./customer.json')
      const customer = JSON.parse(jsonString)
    } catch (err) {
      console.log(err)
      return
    }
    }
  
    jsonReader(filePath:string, cb: Function) {
      fs.readFile(filePath, (err, fileData) => {
        if (err) {
          return cb && cb(err);
        }
        try {
          const object = JSON.parse(fileData)
          return cb && cb(null, object)
        } catch (err) {
          return cb && cb(err)
        }
      })
    }
  */

}


/*

export const readDataTest = () => {
  const fs = require("fs");
  Read users.json file
  fs.readFile("users.json", function (err: Error, data:any) {

      Check for errors
      if (err) throw err;

      Converting to JSON
      const users = JSON.parse(data);

      console.log(users); Print users
  });
}

*/