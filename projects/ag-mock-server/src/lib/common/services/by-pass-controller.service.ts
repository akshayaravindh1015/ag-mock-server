import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { SnackBarService } from '../../components/snack-bar/snack-bar.service';
import { TEMP_JSON } from '../tempSwagger';
import { DataService } from './data.service';
// import * as YAML from 'yamljs';
// import { parse } from "yamljs"

@Injectable()
export class ByPassControllerService {

  selectedFile!: File;
  private jsonAvailSubj = new BehaviorSubject<boolean>(false);
  public jsonAvailAsObs$ = this.jsonAvailSubj.asObservable();

  constructor(
    private _dataServc: DataService,
    private _snackBarServc: SnackBarService,
  ) {
    // this.getPrevDataIfAny();
  }

  getPrevDataIfAny() {
    let sessionString: string = sessionStorage.getItem('proxyMockServerTempResponse') != null ? `${sessionStorage.getItem('proxyMockServerTempResponse')}` : '';
    if (!!sessionString) {
      this._dataServc.apisAr = JSON.parse(sessionString);
      console.log(this._dataServc.apisAr)
      this.jsonAvailSubj.next(true);
    }
  }
  saveInSessionStorage() {
    sessionStorage.setItem('proxyMockServerTempResponse', JSON.stringify(this._dataServc.apisAr));
  }

  createSomeInit() {
    this._dataServc.apisAr = TEMP_JSON;
    this._snackBarServc.showSuccess("Loaded Previous Data Successfully");
    this.jsonAvailSubj.next(true);
  }

  onFileChanged(event: any): Observable<boolean> {
    let fileLoadedSubject: Subject<boolean> = new Subject();
    if ((event != null) && event.target != null) {
      this.selectedFile = event.target.files[0];
      const fileReader = new FileReader();
      fileReader.readAsText(this.selectedFile, "UTF-8");
      fileReader.onload = () => {
        let dataToParse = fileReader.result ? fileReader.result as string : '';
        // console.log(parse(dataToParse));
        if (this._dataServc.loadNewData(dataToParse)) {
          fileLoadedSubject.next(true);
          fileLoadedSubject.complete();
          this._snackBarServc.showSuccess("JSON Updated successfully.");
          this.jsonAvailSubj.next(true);
        } else if (this.parseYAML(dataToParse)) {
          fileLoadedSubject.next(true);
          fileLoadedSubject.complete();
          this._snackBarServc.showSuccess("SWAGGER Uploaded successfully.");
          this.jsonAvailSubj.next(true);
        } else {
          fileLoadedSubject.next(false);
          fileLoadedSubject.complete();
          this._snackBarServc.showError("Enter a Valid JSON file .. or check for any other error");
        }
      }
      fileReader.onerror = (error) => {
        fileLoadedSubject.next(false);
        fileLoadedSubject.complete();
        console.error(error);
        this._snackBarServc.showError("File Read error : Enter a Valid JSON file .. or check for any other error");
      }
    }
    return fileLoadedSubject.asObservable();
  }

  parseYAML(input: string) {
    let lines: string[] = input.split('\n');  // create array where each YAML line is one entry
    let object: { [s: string]: any } = {};
    lines.forEach((line: string) => {
      let delimiter = line.indexOf(':');  // find the colon position inside the line string 
      if (delimiter != -1) {
        let key = line.substr(0, delimiter - 1).trim();  // extract the key (everything before the colon)
        if (this.doNotIngoreSwaggerKey(key)) {
          let value = line.substr(delimiter + 1).trim();  // extract the value (everything after the colon)
          object[key] = value;  // add a new key-value pair to the object
        }
      }
    });
    return true;
  }

  doNotIngoreSwaggerKey(key: string) {
    return !['description'].includes(key);
  }

}
