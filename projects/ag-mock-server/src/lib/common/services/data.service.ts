import { Injectable, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { API, ValueType } from '../models';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public apisAr: { [s: string]: API } = {};

  public globalEditController = new FormControl(true);
  public canEditAsAsync$ = this.globalEditController.valueChanges.pipe(map(val => !!val));
  public apisUpdated: Subject<boolean> = new Subject();
  public apisUpdated$ = this.apisUpdated.asObservable();

  constructor() {
    this.globalEditController.valueChanges.subscribe(_ => {
      this.apisUpdated.next(true)
    });
  }

  public addANewAPI() {
    this.apisAr[(Date.now()).toString() + '_' + Math.random().toString(36).substr(2, 9)] = new API();
    this.apisUpdated.next(true);
  }

  /** Main Functions */
  loadNewData(dataToParse: string): boolean {
    try {
      if (!!dataToParse) {
        this.apisAr = JSON.parse(dataToParse);
        this.saveInSessionStorage();
        this.apisUpdated.next(true);
        return true;
      } else {
        return false;
      }
    }
    catch (error) {
      return false;
    }
  }
  saveInSessionStorage() {
    sessionStorage.setItem('proxyMockServerTempResponse', JSON.stringify(this.apisAr));
  }

  /** Checks */
  public checkIfTheApiExists(request: API): boolean {
    for (const api of Object.values(this.apisAr)) {
      if (api.endPoint.toLowerCase() == request.endPoint.toLowerCase() && request.type == api.type) {
        return true;
      }
    }
    return false;
  }


  /** Getters & Setters */
  get apiIds(): string[] {
    return Object.keys(this.apisAr);
  }
  get canEdit(): boolean {
    return this.globalEditController.value as boolean || true;
  }

  /** Value Type Specific Functions */
  isJson(str: string): boolean {
    //   {"one": {"dfdsfd":"dfdf", "dfdsfdfad":"dfdf", "key": {"dfdsfdfad":"dfdf", "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "two": {"dfdsfd":"dfdf", "dfdsfdfad":"dfdf", "key": {"dfdsfdfad":"dfdf", "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "three": {"dfdsfd":"dfdf", "dfdsfdfad":"dfdf", "key": {"dfdsfdfad":"dfdf", "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "four": {"dfdsfd":"dfdf", "dfdsfdfad":"dfdf", "key": {"dfdsfdfad":"dfdf", "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}, "asdfdfd":"dfdf", "dfddsf":"dfdf", "dfsdfd":45433, "dfsdfd":"dfdf", "dfdsfd":"dfdf","dsdffd":true}}
    try {
      JSON.parse(str);
      return true;
    } catch (e) {
      return false;
    }
  }
  isBoolean(str: string): boolean {
    return str == 'true' || str == 'false';
  }
  getValueType(val: string): ValueType {
    switch (true) {
      case this.isBoolean(val):
        return ValueType.BOOLEAN;
      case !isNaN(+val):
        return ValueType.NUMBER;
      case this.isJson(val):
        return ValueType.JSON;
      default:
        return ValueType.STRING;
    }
  }
  getValueBasedOnValueType(val: string): any {
    switch (true) {
      case this.isBoolean(val):
        return this.isBoolean(val);
      case !isNaN(+val):
        return +val;
      case this.isJson(val):
        return JSON.parse(val);
      default:
        return `${val}`;
    }
  }

}



/*

  In Constructor
  setInterval(() => {
      this.apisAr;
      this.cerateDownloadJsonUri();
    }, 1000);

  Other functions

  generateDownloadJsonUri() {
    let theJSON = JSON.stringify(this.apisAr);
    let uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
    this.downloadJsonHref = uri;
  }
  cerateDownloadJsonUri() {
    let theJSON = JSON.stringify(this.apisAr);
    let blob = new Blob([theJSON], { type: 'text/json' });
    let url = window.URL.createObjectURL(blob);
    let uri: SafeUrl = this.sanitizer.bypassSecurityTrustUrl(url);
    this.downloadJsonHref = uri;
  }

*/

// let val = Object.values(METHOD).reduce(
//   (prev, val) => {
//     return prev + "." + val.toLowerCase() + " { \
//      }\
//     ";
//   }, '');