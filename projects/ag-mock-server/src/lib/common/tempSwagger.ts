import { API, METHOD, ValueType } from "./models";

export const TEMP_JSON: { [s: string]: API } = {
    "k3q8hi1jc": {
        "body": '{"payload":[{"data":{"orderId":"dsfdsfdsfadsfasdfsdfd","item":"Biryani","total_price":180}}]}',
        "endPoint": "/example/put/order",
        "endPointRegEx": new RegExp("/example/put/order"),
        "type": METHOD.PUT,
        "currentResponseCode": 410,
        "parameters": [
            {
                "valueType": ValueType.BOOLEAN,
                "name": "paymentDone",
                "value": "true"
            }
        ],
        "responseHeaders": [],
        "responses": [
            {
                "bodyType": ValueType.STRING,
                "status": 200,
                "body": "Biryani Ordered Successfully"
            },
            {
                "bodyType": ValueType.STRING,
                "status": 404,
                "body": "URL does not exist"
            },
            {
                "bodyType": ValueType.JSON,
                "status": 410,
                "body": `{"message": "Unable to place the order", "reason" : "some Issue with payment source"}`
            }
        ]
    },
    "vp7xd1nwb": {
        "body": 'null',
        "endPoint": "/example/all/orders",
        "endPointRegEx": new RegExp("/example/all/orders"),
        "type": METHOD.GET,
        "currentResponseCode": 200,
        "parameters": [
            {
                "valueType": ValueType.BOOLEAN,
                "name": "onlyRecent",
                "value": "true"
            }
        ],
        "responseHeaders": [],
        "responses": [
            {
                "bodyType": ValueType.JSON,
                "status": 200,
                "body": `[{ "name": "Biryani", "total_price": 180 }, { "name": "Club Sandwich", "total_price": 220 }, { "name": "Momos", "total_price": 120 }, { "name": "Ragi Sangati", "total_price": 90 }]`
            },
            {
                "bodyType": ValueType.JSON,
                "status": 401,
                "body": `{ "message" : "You don\'t permissions for this action"}`
            }
        ]
    },
    "xtac2g6lj": {
        "body": 'null',
        "endPoint": "/example/delete/order",
        "endPointRegEx": new RegExp("/example/delete/order"),
        "type": METHOD.DELETE,
        "currentResponseCode": 204,
        "parameters": [
            {
                "valueType": ValueType.STRING,
                "name": "orderId",
                "value": "sdkfjalk9873498oruhf*09?w*&lkenr"
            }
        ],
        "responseHeaders": [],
        "responses": [
            {
                "bodyType": ValueType.STRING,
                "status": 204,
                "body": "Deletion Success"
            },
            {
                "bodyType": ValueType.JSON,
                "status": 402,
                "body": `{"message" : "Error in deleting the entry", "reason" : "Entry does not exist"}`
            }
        ]
    }
};