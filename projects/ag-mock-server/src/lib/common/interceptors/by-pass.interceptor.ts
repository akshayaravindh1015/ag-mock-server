import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpHeaders, } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { API } from '../models';
import { DataService } from '../services/data.service';
import { SnackBarService } from '../../components/snack-bar/snack-bar.service';

@Injectable()
export class ByPassInterceptor implements HttpInterceptor {

  constructor(
    private _dataServc: DataService,
    private _snackBarServc: SnackBarService,
  ) { }

  intercept(request: HttpRequest<unknown>): Observable<HttpEvent<unknown>> {
    let api = this.checkIfTheReqExists(request) as API;
    if (!!api) {
      const response = api.responses.filter(resp => resp.status == api.currentResponseCode)[0];
      // const headerToAdd: string | { [name: string]: string | string[] } = api.responseHeaders
      //   .filter(el => el.isInUse)
      //   .reduce((acmltr, currEl) => {
      //     // console.log({ ``: el.value})
      //     const keyVal = `${currEl.key}`;
      //     let newHeader: string | { [name: string]: string | string[] } = {};
      //     newHeader[keyVal] = currEl.value;
      //     return { ...acmltr, ...newHeader };
      //   }, {});
      // let headers = new HttpHeaders(headerToAdd);
      // for (const header of api.responseHeaders) {
      //   if (header.isInUse) {
      //     headers.set(header.key, header.value);
      //   }
      // }
      const byPassedResp = new HttpResponse({
        status: response.status,
        // headers: headers,
        body: new HttpResponse({
          status: response.status,
          // headers: headers,
          body: this._dataServc.getValueBasedOnValueType(response.body),
        }),
      });
      return of(byPassedResp);
    }
    return of(
      new HttpResponse(
        { status: 404 }
      )
    );
  }

  private checkIfTheReqExists(request: HttpRequest<unknown>): boolean | API {
    for (const api of Object.values(this._dataServc.apisAr)) {
      let testEndPonint = `${api.endPoint}`;
      const regEx = new RegExp('\{(.*?)\}');
      let tempArr = testEndPonint.match(regEx) != null ? testEndPonint.match(regEx) : [];
      while (tempArr?.length != 0) {
        const toReplace: string = tempArr ? `${tempArr[0]}` : '*';
        testEndPonint = testEndPonint.replace(toReplace, '_______');
        tempArr = testEndPonint.match(regEx) != null ? testEndPonint.match(regEx) : [];
      }
      let newRegExPAtterToCheck = new RegExp(testEndPonint.split('/').join('\\\/').split('_______').join('([a-zA-Z0-9]*?)'));
      if (newRegExPAtterToCheck.test(request.urlWithParams.toLowerCase())) {
        if (request.method == api.type) {
          return api;
        }
        const message = `For "${api.endPoint}" we have response for "${api.type.toUpperCase()}" method, but not for "${request.method.toUpperCase()}" method.`;
        this._snackBarServc.showError(message);
        return false;
      }
    }
    const message = `The entry for this URL does not exist in the Mock_Server_API entries.`;
    this._snackBarServc.showError(message);
    return false;
  }

}



/*

public execute = setTimeout(() => {
  console.log("before to show the element");
  this._loadMain.openAPIConfigScreen();
  console.log("after to show the element");
}, 2000);

*/