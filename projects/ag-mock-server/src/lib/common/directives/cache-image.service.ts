import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

interface CachedImage {
  url: string;
  blob: Blob;
}
@Injectable({
  providedIn: 'root'
})
export class CacheImageService {

  private keySessionStore = "agMockServerCachedImages";
  private _cachedImages: CachedImage[] = [];
  private _cacheUrls: string[] = [];
  private dataLoaded: boolean = false;
  constructor(
    private http: HttpClient,
  ) { }

  /** Main Method */
  public getImage(url: string): Observable<any> {
    this.updateAndLoadData();
    const index = this._cachedImages.findIndex(image => image.url === url);
    if (index > -1) {
      const image = this._cachedImages[index];
      return of(URL.createObjectURL(image.blob));
    }
    return this.http.get(url, { responseType: 'blob' }).pipe(
      tap(blob => this.checkAndCacheImage(url, blob))
    );
  }
  private checkAndCacheImage(url: string, blob: Blob) {
    if (this._cacheUrls.indexOf(url) > -1) {
      this._cachedImages.push({ url, blob });
    }
  }
  private updateAndLoadData() {
    if (!this.dataLoaded) {
      sessionStorage.getItem(this.keySessionStore);
    } else {
      sessionStorage.setItem(this.keySessionStore, JSON.stringify(this.cachedImages));
    }
  }

  /** Getters and Setters */
  addCacheUrl(url: string) {
    this._cacheUrls.push(url);
    // this.cachedImages[url] = 
  }
  set cacheUrls(urls: string[]) {
    this._cacheUrls = [...urls];
  }
  get cacheUrls(): string[] {
    return this._cacheUrls;
  }
  set cachedImages(image: CachedImage) {
    this._cachedImages.push(image);
  }

}
