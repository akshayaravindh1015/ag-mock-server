import { Directive, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { CacheImageService } from './cache-image.service';

@Directive({
  selector: '[cacheImgSrc]'
})
export class CacheImageDirective implements OnInit {

  @Input() cacheImgSrc!: string;
  @Input() spinnerSrc?: string;

  @ViewChild('img', { static: true }) image!: ElementRef;

  constructor(
    private el: ElementRef,
    private _cacheImageServc: CacheImageService,
  ) {
    this.cacheImgSrc = el.nativeElement.src;
    el.nativeElement.src = this.spinnerSrc;
  }

  ngOnInit() {
    this._cacheImageServc.addCacheUrl(this.cacheImgSrc);
    this._cacheImageServc
      .getImage(this.cacheImgSrc)
      .subscribe(
        res => {
          console.log(res);
          this.image.nativeElement.src = this.cacheImgSrc;
        });
  }

  /** To Show loading until Image Loading is done */
  // @HostListener('load') onLoad() {
  //   this.el.nativeElement.src = this.imgSrc;
  // }

}
