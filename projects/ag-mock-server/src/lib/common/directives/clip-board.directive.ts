import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[clipBoard]'
})
export class ClipBoardDirective {

  @Input("clipBoard")
  public payload!: string;

  @Output("copied")
  public copied: EventEmitter<string> = new EventEmitter<string>();

  @HostListener("click", ["$event"])
  public onClick(event: MouseEvent): void {
    event.preventDefault();
    if (!this.payload) {
      return;
    }
    this.copyToClipboard(this.payload);
  }

  copyToClipboard(item: string): void {
    let listener = (e: ClipboardEvent) => {
      e.clipboardData ? e.clipboardData.setData('text/plain', (item)) : '';
      e.preventDefault();
    };
    document.addEventListener('copy', listener);
    document.execCommand('copy');
    document.removeEventListener('copy', listener);
  }

}
