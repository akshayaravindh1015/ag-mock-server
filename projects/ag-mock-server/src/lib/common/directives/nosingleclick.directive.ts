import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[nosingleclick]'
})
export class NosingleclickDirective {

  constructor() { }
  
  @HostListener('click', ['$event'])
  onClick(event: Event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
  }

}
