import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NosingleclickDirective } from './nosingleclick.directive';
import { ClipBoardDirective } from './clip-board.directive';
import { CacheImageDirective } from './cache-image.directive';

@NgModule({
  declarations: [
    NosingleclickDirective,
    ClipBoardDirective,
    CacheImageDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NosingleclickDirective,
    ClipBoardDirective,
    CacheImageDirective,
  ]
})
export class DirectivesModule { }
