import { Output, Component, EventEmitter } from '@angular/core';
import { LoadMainService } from './common/services/load-main.service';

@Component({
  selector: "ag-mock-server-lib",
  templateUrl: "ag-mock-server.components.html",
  styleUrls: ["ag-mock-server.components.scss"],
})
export class AgMockServerComponent {

  @Output()
  closed = new EventEmitter<void>();

  constructor(
    private _loadMainServc: LoadMainService,
  ) { }

  isConfigVisisble: boolean = false;
  changeState(val: boolean) {
    if (val) {
      this._loadMainServc.openAPIConfigScreen();
      this._loadMainServc.closeAPIConfigScreen();
      this._loadMainServc.openAPIConfigScreen();
    } else {
      this._loadMainServc.closeAPIConfigScreen();
    }
    this.isConfigVisisble = val;
    // this.ref.detectChanges();
  }

}
