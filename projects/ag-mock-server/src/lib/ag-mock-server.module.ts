import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgMockServerComponent } from './ag-mock-server.component';
import { ComponentsModule } from './components/components.module';
import { SwaggerUiModule } from './swagger-ui/swagger-ui.module';

@NgModule({
  declarations: [
    AgMockServerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    SwaggerUiModule,
  ],
  exports: [
    AgMockServerComponent
  ],
  providers: [ ]
})
export class AgMockServerModule { }
