import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewComponent } from './view/view.component';
import { ComponentsModule } from '../components/components.module';
import { ApiCardComponent } from './api-card/api-card.component';
import { FormsModule } from '@angular/forms';
import { LandingComponent } from './landing/landing.component';
import { MainComponent } from './main/main.component';

@NgModule({
  declarations: [
    ViewComponent,
    ApiCardComponent,
    LandingComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
  ],
  exports: [
    ViewComponent,
    ApiCardComponent,
    LandingComponent,
    MainComponent,
  ]
})
export class SwaggerUiModule { }
