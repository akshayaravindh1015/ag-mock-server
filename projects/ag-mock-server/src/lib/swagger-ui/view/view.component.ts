import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit } from '@angular/core';
import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewComponent implements OnInit {

  constructor(
    public _dataServc: DataService,
    // private ref: ChangeDetectorRef,,
  ) {
    this._dataServc.apisUpdated$.subscribe(
      () => {
        // this.ref.detectChanges()
      });
  }

  ngOnInit(): void {
    window.onbeforeunload = (event: Event) => {
      this.unloadHandler(event);
    }
  }

  addAPI() {
    this._dataServc.addANewAPI();
  }

  /** HostListners **/
  // @HostListener("window:beforeunload", ["$event"])
  @HostListener("unload", ["$event"])
  unloadHandler(event: Event) {
    this._dataServc.saveInSessionStorage();
  }

}
