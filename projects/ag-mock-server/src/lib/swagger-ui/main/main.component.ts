import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit, EventEmitter, HostBinding, Output } from '@angular/core';

import { fadeInAnimation } from '../../common/animations';
import { ByPassControllerService } from '../../common/services/by-pass-controller.service';

@Component({
  selector: 'lib-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [
    trigger('state', [
      state('opened', style({
        transform: 'translateY(0%)',
        opacity: 0,
      })),
      state('void, closed', style({
        transform: 'translateY(100%)',
        opacity: 1,
      })),
      transition('* => *', animate('100ms ease-in')),
    ]),
    fadeInAnimation,
  ],
  host: { '[@fadeInAnimation]': '' },
})
export class MainComponent implements OnInit {

  @HostBinding('@state')
  state: 'opened' | 'closed' = 'closed';

  @Output()
  closed = new EventEmitter<void>();

  constructor(
    public _controllerServc: ByPassControllerService,
  ) { }

  ngOnInit(): void { }

}
