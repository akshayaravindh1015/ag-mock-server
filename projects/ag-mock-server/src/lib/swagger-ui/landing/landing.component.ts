import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { delay, tap } from 'rxjs/operators';
import { ByPassControllerService } from '../../common/services/by-pass-controller.service';
import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LandingComponent implements OnInit {

  constructor(
    // private ref: ChangeDetectorRef,,
    private _controllerServc: ByPassControllerService,
  ) { }

  ngOnInit(): void {
  }

  startANewSwagger() {
    this._controllerServc.createSomeInit();
    // this.ref.detectChanges();
  }
  onFileChanged(event: any) {
    this._controllerServc.onFileChanged(event)
      .pipe(
        delay(1000),
        tap(success => {
          if (success) {
            // this.ref.detectChanges();
          }
        })
      ).subscribe().unsubscribe();
  }

}
