import { HttpStatusCode, ResponseHeader } from '../../common/models';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { API, METHOD, Parameter, Response, ValueModel } from '../../common/models';
import { DataService } from '../../common/services/data.service';
import { SnackBarService } from '../../components/snack-bar/snack-bar.service';

@Component({
  selector: 'api-card',
  templateUrl: './api-card.component.html',
  styleUrls: ['./api-card.component.scss'],
})
export class ApiCardComponent implements OnInit {

  @Input() apiId!: string;
  public isCardOpen: boolean = false;

  constructor(
    public _dataServc: DataService,
    private _snackBarServc: SnackBarService,
    private ref: ChangeDetectorRef,
  ) { }

  ngOnInit(): void { }

  /** Other Handlers */
  toggleCard() {
    this.isCardOpen = !this.isCardOpen;
  }

  deleteAPI(event: MouseEvent) {
    event.stopImmediatePropagation();
    delete this._dataServc.apisAr[this.apiId];
    this.ref.detach();
  }

  /** Params Specific */
  addParam(key: string = 'new_param', value: string = 'new_param_value') {
    this.api.parameters.push(new Parameter(key, value));
  }
  onParamUpdate(value: ValueModel, index: number) {
    this.api.parameters[index].value = value.textInside;
    this.api.parameters[index].valueType = value.valueType;
  }
  onParamKeyUpdate(value: ValueModel, index: number) {
    this.api.parameters[index].name = value.textInside;
  }
  deleteParam(index: number) {
    this.api.parameters.splice(index, 1);
  }

  /** Responses Specific */
  addResponse() {
    this.api.responses.push(new Response(HttpStatusCode.Gone, '{"message": "Some Error Occured"}'));
  }
  onResponseUpdate(value: ValueModel, index: number) {
    this.api.responses[index].body = value.textInside;
    this.api.responses[index].bodyType = value.valueType;
  }
  deleteResponse(index: number) {
    if (this.api.currentResponseCode == this.api.responses[index].status) {
      const message = `Deselect this response as Current Response Code to delete it. i.e. Select another Current response code`
      console.error(message);
      return;
    }
    this.api.responses.splice(index, 1);
  }
  currentResponseChanged(event: HttpStatusCode) {
    this.api.currentResponseCode = event;
  }
  /** Response Headers */
  addRespHeader() {
    this.api.responseHeaders.push(new ResponseHeader("new_key", "new_resp_header_value"));
  }
  onRespHeaderValueUpdate(value: ValueModel, index: number) {
    this.api.responseHeaders[index].value = value.textInside;
  }
  onRespHeaderKeyUpdate(value: ValueModel, index: number) {
    this.api.responseHeaders[index].key = value.textInside;
  }
  onRespHeaderAvailCahnge(value: boolean, index: number) {
    console.log(`${index} : ${value}`);
    this.api.responseHeaders[index].isInUse = value;
  }
  deleteRespHeader(index: number) {
    this.api.responseHeaders.splice(index, 1);
  }

  /** Status Specific */
  statusChanged(event: HttpStatusCode, index: number) {
    if (this.api.currentResponseCode == this.api.responses[index].status) {
      this.api.currentResponseCode = event;
    }
    this.api.responses[index].status = event;
  }

  /** onBodyUpdate Specific */
  onBodyUpdate(value: ValueModel) {
    this.api.body = value.textInside;
  }

  /** Method and EndPoint - chance for clash */
  methodChanged(val: METHOD) {
    if (this._dataServc.checkIfTheApiExists({ ...this.api, type: val })) {
      this._snackBarServc.showError('The combination you are trying to configure exists already');
    } else {
      this.api.type = val;
    }
  }
  changeEndPoint(endPoint: ValueModel) {
    if (this._dataServc.checkIfTheApiExists({ ...this.api, endPoint: `${endPoint.textInside}` })) {
      this._snackBarServc.showError('The combination you are trying to configure exists already. Update failed.. Refresh the UI.');
    } else {
      this.api.endPoint = `${endPoint.textInside}`;
      this.api.endPointRegEx = new RegExp(this.api.endPoint);
      let testEndPonint = `${endPoint.textInside}`;
      const regEx = new RegExp('\{(.*?)\}');
      let tempArr = testEndPonint.match(regEx) != null ? testEndPonint.match(regEx) : [];
      while (tempArr?.length != 0) {
        const toReplace: string = tempArr ? `${tempArr[0]}` : '*';
        const toReplaceKeyVal: string = toReplace.replace('{', '').replace('}', '');
        if (this.api.parameters.findIndex(el => el.name == toReplaceKeyVal) == -1) {
          this.addParam(toReplaceKeyVal, "New_param_value_came_from_URL");
        }
        testEndPonint = testEndPonint.replace(toReplace, '*****');
        tempArr = testEndPonint.match(regEx) != null ? testEndPonint.match(regEx) : [];
      }
      this.api.endPointRegEx = new RegExp(testEndPonint.split('*****').join('\{(.*?)\}'));
    }
  }

  /** Getters and Setters */
  get api(): API {
    return this._dataServc.apisAr[this.apiId];
  }
  get method(): string {
    return this.api.type;
  }
  get currentAvailStatuses(): Array<number> {
    return this.api.responses.map(val => val.status);
  }

}


/*

const regEx = new RegExp('(?<=\{})(.*?)(?=\})');
const lasttestRegE = new RegExp('(?<=\{)[^\}]*(?=\})');
const lasttestRegExp2 = new RegExp('(?<={)[^)]+(?=})');

*/