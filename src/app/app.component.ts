import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  
  constructor() { }
  title = 'projects/ag-mock-server/src/public-api';
}
