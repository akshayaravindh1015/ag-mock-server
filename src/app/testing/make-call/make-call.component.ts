import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, HostListener } from '@angular/core';
import { Parameter, HttpStatusCode, METHOD, Response } from 'projects/ag-mock-server/src/lib/common/models';
import { API, ValueModel } from 'projects/ag-mock-server/src/public-api';
import { MakeCallService } from './make-call.service';

@Component({
  selector: 'make-call',
  templateUrl: './make-call.component.html',
  styleUrls: ['make-call.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MakeCallComponent implements OnInit {

  public api: API;

  changeState(): void {
    this._makeCallServc.saveInSessionStorage(this.api);
    this.ref.detectChanges();
  }

  constructor(
    private ref: ChangeDetectorRef,
    private _makeCallServc: MakeCallService,
  ) {
    this.api = this._makeCallServc.getPrevAPICallIfAny();
  }

  ngOnInit(): void { }

  makeTheAPICall() {
    this._makeCallServc.makeAPICall(this.api);
  }

  /** Other Handlers */
  addParam() {
    this.api.parameters.push(new Parameter("new_param", "new_param_value"));
  }
  addResponse() {
    this.api.responses.push(new Response(HttpStatusCode.Gone, "new_param_value"));
  }
  onParamUpdate(value: ValueModel, index: number) {
    this.api.parameters[index].value = value.textInside;
    this.api.parameters[index].valueType = value.valueType;
  }
  onParamKeyUpdate(value: ValueModel, index: number) {
    this.api.parameters[index].name = value.textInside;
  }
  statusChanged(event: HttpStatusCode, index: number) {
    this.api.responses[index].status = event;
  }
  onResponseUpdate(value: ValueModel, index: number) {
    this.api.responses[index].body = value.textInside;
    this.api.responses[index].bodyType = value.valueType;
  }
  methodChanged(val: METHOD) {
    this.api.type = val;
    this.ref.detectChanges();
  }
  changeEndPoint(endPoint: ValueModel) {
    this.api.endPoint = `${endPoint.textInside}`;
  }

  /** HostListners **/
  @HostListener("unload", ["$event"])
  unloadHandler(event: Event) {
    this._makeCallServc.saveInSessionStorage(this.api);
  }

  /** Getters and Setters */
  get method(): string {
    return this.api.type;
  }

}
