import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Parameter, METHOD } from 'projects/ag-mock-server/src/lib/common/models';
import { API, DataService } from 'projects/ag-mock-server/src/public-api';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MakeCallService {

  constructor(
    private http: HttpClient,
    private _dataServc: DataService,
  ) { }

  saveInSessionStorage(api: API) {
    sessionStorage.setItem('testAPICall', JSON.stringify(api));
  }
  getPrevAPICallIfAny(): API {
    let sessionString: string = sessionStorage.getItem('testAPICall') != null ? `${sessionStorage.getItem('testAPICall')}` : '';
    if (!!sessionString) {
      return JSON.parse(sessionString);
    }
    return new API('/endpoint/post/{one}/dfs/{two}', METHOD.POST);
  }

  /** Main Functions */
  makeAPICall(api: API) {
    this.getApiCallObservable(api)
      .subscribe(
        (resp: HttpResponse<any>) => {
          console.log(resp);
          console.log(resp.headers.keys())
        },
        (error: Error) => {
          console.error(error);
          console.error("Error has occured while making the API call.")
        }
      );
  }

  /** Private functions */
  getHeadersFromParams(params: Parameter[]): HttpHeaders {
    let headers = new HttpHeaders({ "test": "test_Header_Value" });
    params.forEach(
      (param) => {
        headers.append(param.name, this._dataServc.getValueBasedOnValueType(param.value));
      }
    );
    return headers;
  }
  getApiCallObservable(api: API): Observable<HttpResponse<any>> {
    api.endPoint = api.endPoint.replace("{", "").replace("}", "");
    switch (api.type) {
      case METHOD.POST:
        return this.http.post<HttpResponse<any>>(
          api.endPoint, {
          headers: this.getHeadersFromParams(api.parameters),
          observe: 'body',
        });
      case METHOD.PUT:
        return this.http.put<HttpResponse<any>>(
          api.endPoint, {
          headers: this.getHeadersFromParams(api.parameters),
          observe: 'body',
        });
      case METHOD.DELETE:
        return this.http.delete<HttpResponse<any>>(
          api.endPoint, {
          headers: this.getHeadersFromParams(api.parameters),
          observe: 'body',
        });
      case METHOD.GET:
      default:
        return this.http.get<HttpResponse<any>>(
          api.endPoint, {
          headers: this.getHeadersFromParams(api.parameters),
          observe: 'body',
        });
    }
  }
}
