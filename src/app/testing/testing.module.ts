import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MakeCallComponent } from './make-call/make-call.component';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from 'projects/ag-mock-server/src/public-api';

@NgModule({
  declarations: [
    MakeCallComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
  ],
  exports: [
    MakeCallComponent
  ]
})
export class TestingModule { }
