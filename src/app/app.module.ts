import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { TestingModule } from './testing/testing.module';
import { environment } from 'src/environments/environment';
import { ByPassControllerService, ByPassInterceptor, FakeBackendService } from 'projects/ag-mock-server/src/public-api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TestingModule,
  ],
  providers: [
    ...(
      environment.production ? // <--can use any flag other than PROD flag.
        [] : [
          FakeBackendService,
          ByPassControllerService,
          {
            provide: HTTP_INTERCEPTORS,
            useClass: ByPassInterceptor,
            multi: true,
          },
          {
            provide: APP_INITIALIZER,
            useFactory: (service: FakeBackendService) => () => { return service.startMockServer(); },
            deps: [FakeBackendService],
            multi: true,
          }
        ]
    ),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
